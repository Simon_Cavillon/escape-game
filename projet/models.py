from flask_login import UserMixin
import datetime
from . import db

class APPARTENIR(UserMixin, db.Model):
    __tablename__ = 'appartenir'
    groupe_id = db.Column(db.Integer, db.ForeignKey('groupe.id'), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('utilisateur.id'), primary_key=True)
    user = db.relationship("UTILISATEUR", back_populates="groupes")
    groupe = db.relationship("GROUPE", back_populates="users")
	
class RELIER(UserMixin, db.Model):
    __tablename__ = 'relier'
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'), primary_key=True)
    groupe_id = db.Column(db.Integer, db.ForeignKey('groupe.id'), primary_key=True)
    groupe = db.relationship("GROUPE", back_populates="sessions")
    session = db.relationship("SESSION", back_populates="groupes")

class ATTACHER(UserMixin, db.Model):
    __tablename__ = 'attacher'
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'), primary_key=True)
    escapegame_id = db.Column(db.Integer, db.ForeignKey('escapegame.id'), primary_key=True)
    escapegame = db.relationship("ESCAPEGAME", back_populates="sessions")
    session = db.relationship("SESSION", back_populates="escapegames")	

class POSSEDER(UserMixin, db.Model):
    __tablename__ = 'posseder'
    escapegame_id = db.Column(db.Integer, db.ForeignKey('escapegame.id'), primary_key=True)
    salle_id = db.Column(db.Integer, db.ForeignKey('salle.id'), primary_key=True)
    salle = db.relationship("SALLE", back_populates="escapegames")
    escapegame = db.relationship("ESCAPEGAME", back_populates="salles")

class CONTENIR(UserMixin, db.Model):
    __tablename__ = 'contenir'
    salle_id = db.Column(db.Integer, db.ForeignKey('salle.id'), primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'), primary_key=True)
    coordX = db.Column(db.Integer)
    coordY = db.Column(db.Integer)
    question = db.relationship("QUESTION", back_populates="salles")
    salle = db.relationship("SALLE", back_populates="questions")

class RATTACHER(UserMixin, db.Model):
    __tablename__ = 'rattacher'
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'), primary_key=True)
    reponse_id = db.Column(db.Integer, db.ForeignKey('reponse.id'), primary_key=True)
    reponse = db.relationship("REPONSE", back_populates="questions")
    question = db.relationship("QUESTION", back_populates="reponses")

class AVOIR(UserMixin, db.Model):
    __tablename__ = 'avoir'
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'), primary_key=True)
    enregistrement_id = db.Column(db.Integer, db.ForeignKey('enregistrement.id'), primary_key=True)
    enregistrement = db.relationship("ENREGISTREMENT", back_populates="questions")
    question = db.relationship("QUESTION", back_populates="enregistrements")

class ENREGISTRER(UserMixin, db.Model):
    __tablename__ = 'enregistrer'
    enregistrement_id = db.Column(db.Integer, db.ForeignKey('enregistrement.id'), primary_key=True)
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'), primary_key=True)
    session = db.relationship("SESSION", back_populates="enregistrements")
    enregistrement = db.relationship("ENREGISTREMENT", back_populates="sessions")

class ASSOCIER(UserMixin, db.Model):
    __tablename__ = 'associer'
    professeur_id = db.Column(db.Integer, db.ForeignKey('professeur.id'), primary_key=True)
    escapegame_id = db.Column(db.Integer, db.ForeignKey('escapegame.id'), primary_key=True)
    escapegame = db.relationship("ESCAPEGAME", back_populates="professeurs")
    professeur = db.relationship("PROFESSEUR", back_populates="escapegames")

# Classe Parent
class UTILISATEUR(UserMixin, db.Model):
    __tablename__ = 'utilisateur'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    dateCreation = db.Column(db.DateTime, default=datetime.datetime.now())
    nom = db.Column(db.String(1000))
    type = db.Column(db.Text, nullable=False)
    groupes = db.relationship("APPARTENIR", back_populates="user")
    __mapper_args__ = {'polymorphic_on': type}

# Classe Enfant	
class ELEVE(UTILISATEUR):
    __tablename__ = 'eleve'
    id = db.Column(db.Integer, db.ForeignKey('utilisateur.id'), primary_key=True) # Clé primaire
    classe = db.Column(db.String(100))
    __mapper_args__ = {'polymorphic_identity': 'eleve'}
	
# Classe Parent	
class PROFESSEUR(UTILISATEUR):
    __tablename__ = 'professeur'
    id = db.Column(db.Integer, db.ForeignKey('utilisateur.id'), primary_key=True) # Clé primaire
    discipline = db.Column(db.String(100))
    escapegames = db.relationship("ASSOCIER", back_populates="professeur")
    __mapper_args__ = {'polymorphic_identity': 'professeur'}

# Class PARENT
class GROUPE(UserMixin, db.Model):
    __tablename__ = 'groupe'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    nom = db.Column(db.String(100))
    users = db.relationship("APPARTENIR", back_populates="groupe")
    sessions = db.relationship("RELIER", back_populates="groupe")

# Classe Enfant	
class SESSION(UserMixin, db.Model):
    __tablename__ = 'session'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    dateDebut = db.Column(db.DateTime, default=datetime.datetime.now())
    dateFin = db.Column(db.DateTime)
    etatS = db.Column(db.String(100)) # Valeur : "En Cours" pour une partie en cours, "Pause" pour une partie en pause, "Fini" pour une partie finie
    tempsRestant = db.Column(db.Integer)
    progression = db.Column(db.Integer, default=0)
    note = db.Column(db.String(100))
    nb_questions = db.Column(db.Integer, default=0)
    nb_questions_juste = db.Column(db.Integer, default=0)
    groupes = db.relationship("RELIER", uselist=False, back_populates="session")
    escapegames = db.relationship("ATTACHER", uselist=False, back_populates="session")
    enregistrements = db.relationship("ENREGISTRER", back_populates="session")

# Class PARENT
class ESCAPEGAME(UserMixin, db.Model):
    __tablename__ = 'escapegame'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    nom = db.Column(db.String(1000), unique=True)
    penaliter = db.Column(db.Integer)
    timer = db.Column(db.Integer)
    createurE = db.Column(db.String(100))
    sessions = db.relationship("ATTACHER", back_populates="escapegame")
    salles = db.relationship("POSSEDER", back_populates="escapegame")
    professeurs = db.relationship("ASSOCIER", back_populates="escapegame")

# Classe Enfant/Parent
class SALLE(UserMixin, db.Model):
    __tablename__ = 'salle'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    nomSal = db.Column(db.String(100), unique=True)
    sal_prec = db.Column(db.Integer, db.ForeignKey('salle.id'))
    image = db.Column(db.String(1000))
    escapegames = db.relationship("POSSEDER", uselist=False, back_populates="salle")
    questions = db.relationship("CONTENIR", back_populates="salle")
    prec_id = db.relationship("SALLE", remote_side=[id])

# Classe Enfant/Parent
class QUESTION(UserMixin, db.Model):
    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    titreQ = db.Column(db.String(100))
    contenuQ = db.Column(db.String(500))
    etatQ = db.Column(db.Boolean, default=False) # Etat : "False" Non répondu, "True" répondu
    salles = db.relationship("CONTENIR", back_populates="question")
    reponses = db.relationship("RATTACHER", back_populates="question")
    enregistrements = db.relationship("AVOIR", back_populates="question")

# Classe Enfant/Parent
class REPONSE(UserMixin, db.Model):
    __tablename__ = 'reponse'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    texteR = db.Column(db.String(100))
    correct = db.Column(db.String(100)) # Valeur : "Oui" pour correct, "Non" pour incorrect
    questions = db.relationship("RATTACHER", back_populates="reponse")

# Classe Enfant/Parent
class ENREGISTREMENT(UserMixin, db.Model):
    __tablename__ = 'enregistrement'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    correct = db.Column(db.String(100)) # Valeur : "Oui" pour correct, "Non" pour incorrect
    questions = db.relationship("AVOIR", back_populates="enregistrement")
    sessions = db.relationship("ENREGISTRER", uselist=False, back_populates="enregistrement")

def get_session_by_id(id):
    return SESSION.query.filter(PLAYLIST.id == id)

def get_groupe_utilisateur(id):
    return GROUPE.query.join(APPARTENIR).join(UTILISATEUR).filter(APPARTENIR.user_id == id).all()

def get_questions_salle(id):
    return QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==id).all()

def get_questions_number(id):
    return QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==id).count()

def get_reponses_question(id):
    return REPONSE.query.join(RATTACHER).join(QUESTION).filter(RATTACHER.question_id==id).all()