from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required
from .models import ELEVE, GROUPE, UTILISATEUR, APPARTENIR, SESSION, RELIER, ESCAPEGAME, ATTACHER, SALLE, POSSEDER, QUESTION, CONTENIR, REPONSE, RATTACHER, ENREGISTREMENT, AVOIR, ENREGISTRER, PROFESSEUR
from . import db
from datetime import datetime

auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = UTILISATEUR.query.filter_by(email=email).first()

    # Vérifier si l’utilisateur existe réellement
    # Prendre le mot de passe fourni par l’utilisateur, le hacher et le comparer au mot de passe haché dans la base de données
    if not user or not check_password_hash(user.password, password):
        flash('Veuillez vérifier vos informations de connexion et réessayer.')
        return redirect(url_for('auth.login')) # Si l’utilisateur n’existe pas ou si le mot de passe est incorrect, recharger la page

    # Si la vérification ci-dessus réussit, alors nous savons que l’utilisateur a les bons identifiants
    login_user(user, remember=remember)
    return redirect(url_for('main.index'))

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))
