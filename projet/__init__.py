from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import Blueprint
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager
import click
import os
basedir = os.path.abspath(os.path.dirname(__file__))

# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()



def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'hard to guess string'
    app.config['SQLALCHEMY_DATABASE_URI'] =\
    'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
    app.config['TESTING'] = False
    app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.jpeg']
    app.config['UPLOAD_PATH'] = os.path.join(basedir, 'files')
    app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024




    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)
	
    db.init_app(app)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)
	
    from .models import ELEVE, GROUPE, UTILISATEUR, APPARTENIR, SESSION, RELIER, ESCAPEGAME, ATTACHER, SALLE, POSSEDER, QUESTION, CONTENIR, REPONSE, RATTACHER, ENREGISTREMENT, AVOIR, ENREGISTRER, PROFESSEUR
    @login_manager.user_loader
    def load_user(user_id):
        return UTILISATEUR.query.get(int(user_id))
	
    @app.cli.command("addeleve")
    @click.argument("email")
    @click.argument("password")
    @click.argument("nom")
    @click.argument("classe")
    def addeleve(email, password, nom, classe):
       new_eleve = ELEVE(email=email, nom=nom, password=generate_password_hash(password, method='sha256'), classe=classe)
       db.session.add(new_eleve)
       db.session.commit()
	   
    @app.cli.command("addprofesseur")
    @click.argument("email")
    @click.argument("password")
    @click.argument("nom")
    @click.argument("discipline")
    def addprofesseur(email, password, nom, discipline):
       new_professeur = PROFESSEUR(email=email, nom=nom, password=generate_password_hash(password, method='sha256'), discipline=discipline)
       db.session.add(new_professeur)
       db.session.commit()
	   
    @app.cli.command("addgroupe")
    @click.argument("nom")
    def addgroupe(nom):
       new_groupe = GROUPE(nom=nom)
       db.session.add(new_groupe)
       db.session.commit()

    @app.cli.command("addescapegame")
    @click.argument("nom")
    @click.argument("penaliter")
    @click.argument("timer")
    def addescapegame(nom, penaliter, timer):
       new_escapegame = ESCAPEGAME(nom=nom, penaliter=penaliter, timer=timer)
       db.session.add(new_escapegame)
       db.session.commit()

    @app.cli.command("addsession")
    def addsession():
       timer = db.session.query(ESCAPEGAME.timer).first()
       new_session = SESSION(etatS="En Cours")
       db.session.add(new_session)
       db.session.commit()

    @app.cli.command("addsalle")
    @click.argument("nomsal")
    @click.argument("salle_prec")
    @click.argument("image")
    def addsalle(nomsal, salle_prec, image):
       new_salle = SALLE(nomSal=nomsal, sal_prec=salle_prec, image=image)
       db.session.add(new_salle)
       db.session.commit()

    @app.cli.command("addquestion")
    @click.argument("titre")
    @click.argument("contenu")
    def addquestion(titre, contenu):
       new_question = QUESTION(titreQ=titre, contenuQ=contenu, etatQ=False)
       db.session.add(new_question)
       db.session.commit()

    @app.cli.command("addreponse")
    @click.argument("texte")
    @click.argument("correct")
    def addreponse(texte, correct):
       new_reponse = REPONSE(texteR=texte, correct=correct)
       db.session.add(new_reponse)
       db.session.commit()
	   
    @app.cli.command("addenregistrement")
    def addenregistrement():
       new_enregistrement = ENREGISTREMENT()
       db.session.add(new_enregistrement)
       db.session.commit()

    @app.cli.command("addusergroupe")
    @click.argument("user_id")
    @click.argument("groupe_id")
    def addusergroupe(user_id, groupe_id):
       user = UTILISATEUR.query.get(int(user_id))
       groupe = GROUPE.query.get(int(groupe_id))
       appartenir = APPARTENIR()
       appartenir.user=user
       groupe.users.append(appartenir)
       db.session.add(appartenir)
       db.session.commit()

    @app.cli.command("addsessiongroupe")
    @click.argument("session_id")
    @click.argument("groupe_id")
    def addsessiongroupe(session_id, groupe_id):
       session = SESSION.query.get(int(session_id))
       groupe = GROUPE.query.get(int(groupe_id))
       relier = RELIER()
       relier.session=session
       groupe.sessions.append(relier)
       db.session.add(relier)
       db.session.commit()

    @app.cli.command("addsessionescapegame")
    @click.argument("session_id")
    @click.argument("escapegame_id")
    def addsessionescapegame(session_id, escapegame_id):
       session = SESSION.query.get(int(session_id))
       escapegame = ESCAPEGAME.query.get(int(escapegame_id))
       attacher = ATTACHER()
       attacher.session=session
       escapegame.sessions.append(attacher)
       db.session.add(attacher)
       db.session.commit()
	   
    @app.cli.command("addescapegamesalle")
    @click.argument("escapegame_id")
    @click.argument("salle_id")
    def addescapegamesalle(escapegame_id, salle_id):
       escapegame = ESCAPEGAME.query.get(int(escapegame_id))
       salle = SALLE.query.get(int(salle_id))
       posseder = POSSEDER()
       posseder.salle=salle
       escapegame.salles.append(posseder)
       db.session.add(posseder)
       db.session.commit()

    @app.cli.command("addquestionsalle")
    @click.argument("salle_id")
    @click.argument("question_id")
    @click.argument("coordx")
    @click.argument("coordy")
    def addquestionsalle(salle_id, question_id, coordx, coordy):
       salle = SALLE.query.get(int(salle_id))
       question = QUESTION.query.get(int(question_id))
       contenir = CONTENIR()
       contenir.question=question
       contenir.salle=salle
       contenir.coordX=coordx
       contenir.coordY=-300
       salle.questions.append(contenir)
       db.session.add(contenir)
       db.session.commit()
	   
    @app.cli.command("addreponsequestion")
    @click.argument("question_id")
    @click.argument("reponse_id")
    def addreponsequestion(question_id, reponse_id):
       question = QUESTION.query.get(int(question_id))
       reponse = REPONSE.query.get(int(reponse_id))
       rattacher = RATTACHER()
       rattacher.reponse=reponse
       rattacher.question=question
       question.reponses.append(rattacher)
       db.session.add(rattacher)
       db.session.commit()

    @app.cli.command("addenregistrementreponse")
    @click.argument("reponse_id")
    @click.argument("enregistrement_id")
    def addenregistrementreponse(reponse_id, enregistrement_id):
       reponse = REPONSE.query.get(int(reponse_id))
       enregistrement = ENREGISTREMENT.query.get(int(enregistrement_id))
       avoir = AVOIR()
       avoir.enregistrement=enregistrement
       reponse.enregistrements.append(avoir)
       db.session.add(avoir)
       db.session.commit()

    @app.cli.command("addsessionenregistrement")
    @click.argument("enregistrement_id")
    @click.argument("session_id")
    def addenregistrementreponse(enregistrement_id, session_id):
       enregistrement = ENREGISTREMENT.query.get(int(enregistrement_id))
       session = SESSION.query.get(int(session_id))
       enregistrer = ENREGISTRER()
       enregistrer.session=session
       enregistrement.sessions.append(enregistrer)
       db.session.add(enregistrement)
       db.session.commit()

    @app.cli.command("showelevegroupe")
    @click.argument("groupe_id")
    def showelevegroupe(groupe_id):
       groupe = GROUPE.query.get(int(groupe_id))
       for i in groupe.eleves:
         print(i.eleve.nom)
	   
    @app.cli.command("passwd")
    @click.argument('nom')
    @click.argument('passwd')
    def passwd(nom, passwd):
       eleve = ELEVE.query.filter_by(nom=nom).first()
       if eleve:
         eleve.password = generate_password_hash(passwd, method='sha256')
         db.session.commit()
       else:
         print("****************************************************")
         print("Il n'y a pas de compte associé au nom " + nom)
         print("****************************************************")
	   
    @app.cli.command()
    def syncdb():
       db.create_all(app=create_app())
	   
    return app