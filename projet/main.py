from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Blueprint, render_template, redirect, url_for, request, flash
from .models import get_groupe_utilisateur, ELEVE, GROUPE, UTILISATEUR, APPARTENIR, SESSION, RELIER, ESCAPEGAME, ATTACHER, SALLE, POSSEDER, QUESTION, CONTENIR, REPONSE, RATTACHER, ENREGISTREMENT, AVOIR, ENREGISTRER, PROFESSEUR
from . import db
import datetime
import locale

locale.setlocale(locale.LC_TIME,'')

main = Blueprint('main', __name__)

@main.route('/')
@login_required
def index():
    return render_template('index.html')

#############################
########  PROFILE  ##########
#############################

@main.route('/profile')
@login_required
def profile():
    if current_user.type=="professeur":
       return render_template('profile.html', name=current_user.nom, groupe=current_user.groupes, mail=current_user.email, discipline=current_user.discipline)
    else :
       return render_template('profile.html', name=current_user.nom, mail=current_user.email, classe=current_user.classe)

@main.route('/changePW', methods=['GET', 'POST'])
@login_required
def changePW():
    ancienMDP= request.form.get('old')
    nouveauMDP= request.form.get('new')

    user = UTILISATEUR.query.filter_by(id=current_user.id).first()

    if not user or check_password_hash(user.password, ancienMDP):
        current_user.password=generate_password_hash(nouveauMDP, method='sha256')
        db.session.commit()
        return redirect(url_for('main.index'))
    else:
        flash('Le mot de passe actuel n\'est pas le bon')
        return redirect(url_for('main.password'))

@main.route('/password',methods=['GET', 'POST'])
@login_required
def password():
    return render_template('changepassword.html')


#################################
########  ESCAPE GAME  ##########
#################################

@main.route('/groupe/<id>/session/<sessionid>/escapegame/<escapegameid>', methods=['POST'])
@login_required
def escapegame(id, sessionid, escapegameid):
    salles = SALLE.query.join(POSSEDER).join(ESCAPEGAME).filter(POSSEDER.escapegame_id==escapegameid).all()

    return redirect(url_for('main.salle', id=id, sessionid=sessionid, escapegameid=escapegameid, salleid=salles[0].id))

@main.route('/new_escapegame', methods=['POST'])
@login_required
def escapegame_post():

    form_escapegame_nom = request.form.get('nomE')
    form_escapegame_timer = request.form.get('timer')
    form_escapegame_penaliter = request.form.get('penaliter')
    form_salle_nom = request.form.get('nomSal')
    form_sallePrec_nom = request.form.get('nomSalPrec')
    formimageSal = request.form.get('imageSal')
    escapegame_exist = ESCAPEGAME.query.filter(ESCAPEGAME.nom==form_escapegame_nom).first()
    salle_exist = SALLE.query.filter(SALLE.nomSal==form_salle_nom).first()
    if (salle_exist):
        flash("La salle que vous avez entrer existe déjà !")
        return redirect(url_for('main.init_salle'))
    if (escapegame_exist):
        flash("Cette Escape Game existe déjà !")
        return redirect(url_for('main.init_salle'))
    nouveau_EscapeGame = ESCAPEGAME(nom=form_escapegame_nom, penaliter=form_escapegame_penaliter, timer=form_escapegame_timer, createurE=current_user.nom)
    db.session.add(nouveau_EscapeGame)
    db.session.commit()

    if (form_escapegame_nom==""):
        flash("Vous devez entrer un nom pour votre escape game.")
        return redirect(url_for('main.init_salle'))
    elif (form_salle_nom==""):
        flash("Vous devez entrer un nom pour votre salle initialle.")
        return redirect(url_for('main.init_salle'))
    elif (form_sallePrec_nom==""):
        flash("Vous devez entrer le nom de votre salle précédente. Si c'est la première salle que vous faite pour cette escape game prenez le nom de la salle initialle.")
        return redirect(url_for('main.init_salle'))
    elif (formimageSal==""):
        flash("Vous devez entrer l'URL d'une image de 800x480.")
        return redirect(url_for('main.init_salle'))

		
    salles = SALLE.query.join(POSSEDER).join(ESCAPEGAME).filter(POSSEDER.escapegame_id==nouveau_EscapeGame.id).all()
    compteur_salle=0
    if (salles): # On vient vérifier si la salle précédente existe
      compteur_salle=salles[0].id-1
    else:
      salle2=SALLE.query.all()
      if (salle2):
        salle_actuelle=salle2[len(salle2)-1].id
        compteur_salle=salle_actuelle+1
      else:
        compteur_salle=1

    nouvelle_salle = SALLE(nomSal=form_salle_nom, sal_prec=compteur_salle, image=formimageSal)
    db.session.add(nouvelle_salle)
    db.session.commit()

    escapegame = ESCAPEGAME.query.get(int(nouveau_EscapeGame.id))
    salle = SALLE.query.get(int(nouvelle_salle.id))
    posseder = POSSEDER()
    posseder.salle=salle
    posseder.escapegame=escapegame
    escapegame.salles.append(posseder)
    db.session.add(posseder)
    db.session.commit()

    return redirect(url_for('main.index'))


##############################
#########  GROUPE   ##########
##############################

@main.route('/groupes', methods=['POST', 'GET'])
@login_required
def groupes():
    return render_template('groupes.html', groupes=get_groupe_utilisateur(current_user.id))

@main.route('/groupe')
@login_required
def groupe_post():
    groupe_id=request.form.get('prodId')
    return redirect(url_for('main.groupe', id=groupe_id))

@main.route('/groupe/<id>')
@login_required
def groupe(id):
    users = UTILISATEUR.query.join(APPARTENIR).join(GROUPE).filter(APPARTENIR.groupe_id == id).all()
    sessions = SESSION.query.join(RELIER).join(GROUPE).filter(RELIER.groupe_id==id).all()
    attachers = ATTACHER.query.all()
    escapegames = ESCAPEGAME.query.all()

    return render_template('groupe.html', users=users, sessions=sessions, attachers=attachers, escapegames=escapegames, id=id)

@main.route('/new_groupe', methods=['POST', 'GET'])
@login_required
def new_groupe():
    return render_template('new_groupe.html')

@main.route('/rejoindre_groupe', methods=['POST', 'GET'])
@login_required
def rejoindre_groupe():
    groupes = GROUPE.query.all()
    return render_template('rejoindre_groupe.html', groupes=groupes)

@main.route('/rejoindre_groupe_post', methods=['POST', 'GET'])
@login_required
def rejoindre_groupe_post():
    groupe_nom = request.form.get("groupe", None)
    if (groupe_nom==None or groupe_nom=="Sélectionnez la liste déroulante"):
        flash("Vous n'avez pas sélectionné d'escape game.")
        return redirect(url_for('main.session', id=id))
    groupe = GROUPE.query.filter_by(nom=groupe_nom).first()

    user = UTILISATEUR.query.get(int(current_user.id))
    appartenir = APPARTENIR()
    appartenir.user=user
    with db.session.no_autoflush:
      groupe.users.append(appartenir)
    db.session.add(appartenir)
    db.session.commit()
    return redirect(url_for('main.groupes'))

@main.route('/new_groupe_post', methods=['POST'])
@login_required
def new_groupe_post():

    form_groupe_nom = request.form.get('nom')
    if (form_groupe_nom==""):
        flash("Vous devez entrer un nom.")
        return redirect(url_for('main.new_groupe'))
    groupe_exist = GROUPE.query.filter_by(nom=form_groupe_nom).first()
    if (groupe_exist):
            flash('Vous possédez déjà un autre groupe ayant le même nom.')
            return redirect(url_for('main.new_groupe'))
    nouveau_groupe = GROUPE(nom=form_groupe_nom)
    db.session.add(nouveau_groupe)
    db.session.commit()

    user = UTILISATEUR.query.get(int(current_user.id))
    groupe = GROUPE.query.get(int(nouveau_groupe.id))
    appartenir = APPARTENIR()
    appartenir.user=user
    with db.session.no_autoflush:
      groupe.users.append(appartenir)
    db.session.add(appartenir)
    db.session.commit()

    return redirect(url_for('main.groupes'))

@main.route('/supprimer_groupe', methods=['POST'])
@login_required
def supprimer_groupe():
    id = request.form.get('id')
    groupe = GROUPE.query.get(int(id))
    GROUPE.query.filter(GROUPE.id == id).delete()
    APPARTENIR.query.filter(APPARTENIR.groupe_id == id and APPARTENIR.user_id == current_user.id).delete()
    sessions = SESSION.query.join(RELIER).join(GROUPE).filter(RELIER.groupe_id == groupe.id).all()
    for s in sessions:
      GROUPE.query.get(int(id))
      SESSION.query.get(int(s.id)).delete()
    db.session.commit()

    return redirect(url_for('main.groupes'))

##############################
#########  SESSION   #########
##############################

@main.route('/session/<id>')
@login_required
def session(id):
    escapegames = ESCAPEGAME.query.filter(ESCAPEGAME.createurE==current_user.nom).all()
    return render_template('session.html', escapegames=escapegames, id=id)

@main.route('/new_session', methods=['POST'])
@login_required
def session_post():
    id = request.form.get('prodId')
    escapegame_nom = request.form.get("escapegame", None)
    if (escapegame_nom==None or escapegame_nom=="Sélectionnez la liste déroulante"):
        flash("Vous n'avez pas sélectionné d'escape game.")
        return redirect(url_for('main.session', id=id))
    escapegame = ESCAPEGAME.query.filter_by(nom=escapegame_nom).first()
    temprestant = escapegame.timer
    escapeid = escapegame.id
    session_encours = None
    sessions = SESSION.query.join(RELIER).join(GROUPE).filter(RELIER.groupe_id==id).all()
    session_exist = SESSION.query.join(ATTACHER).join(ESCAPEGAME).filter(ATTACHER.escapegame_id==escapegame.id).first()
    if (session_exist):
        flash("Vous avez déjà créer cette escape game.")
        return redirect(url_for('main.session', id=id))
	
    if (escapegame_nom==None or escapegame_nom=="Sélectionnez la liste déroulante"):
        flash("Vous n'avez pas sélectionné d'escape game.")
        return redirect(url_for('main.session', id=id))
    nouvelle_session = SESSION(tempsRestant=temprestant, etatS="En Cours")

    # Ajouter la nouvelle session à la base de donnée
    db.session.add(nouvelle_session)
    db.session.commit()

	# Ajouter la nouvelle session dans la relation Relier
    escapegame = ESCAPEGAME.query.get(int(escapeid))
    attacher = ATTACHER()
    attacher.session=nouvelle_session
    with db.session.no_autoflush:
      escapegame.sessions.append(attacher)
    db.session.add(attacher)
    db.session.commit()

	# Ajouter la nouvelle session dans la relation Attacher
    groupe = GROUPE.query.get(int(id))
    relier = RELIER()
    relier.session=nouvelle_session
    with db.session.no_autoflush:
      groupe.sessions.append(relier)
    db.session.add(relier)
    db.session.commit()

    return redirect(url_for('main.groupe', id=id))


##############################
#########   SALLE    #########
##############################

@main.route('/init_salle',methods=['POST', 'GET'])
@login_required
def init_salle():
    return render_template('init_salle.html')


@main.route('/addSalle', methods=['POST', 'GET'])
@login_required
def addSalle():
    return render_template('addSalle.html')

@main.route('/groupe/<id>/session/<sessionid>/escapegame/<escapegameid>/salle/<salleid>')
@login_required
def salle(id, sessionid, escapegameid, salleid):
    salle = SALLE.query.get(int(salleid))
    image = salle.image
    session = SESSION.query.get(int(sessionid))
    timer = session.tempsRestant
    progression=session.progression

    questions = QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
    reponses = REPONSE.query.join(RATTACHER).join(QUESTION).join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
    coords = CONTENIR.query.filter(CONTENIR.salle_id==salleid).all()
    rattachers = RATTACHER.query.join(QUESTION).join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
    questionsNumber = QUESTION.query.join(CONTENIR).join(SALLE).join(POSSEDER).join(ESCAPEGAME).filter(POSSEDER.escapegame_id==int(escapegameid)).count()

    return render_template('salle.html', id=id, sessionid=sessionid, escapegameid=escapegameid, salleid=salleid, image=image, timer=timer, questions=questions, progression=progression, rattachers=rattachers, reponses=reponses, coords=coords, questionsNumber=questionsNumber)

@main.route('/groupe/<id>/session/<sessionid>/escapegame/<escapegameid>/salle/<salleid>/changer_salle', methods=['POST', 'GET'])
@login_required
def changer_salle(id, sessionid, escapegameid, salleid):
    salle_prec = SALLE.query.get(int(salleid))
    salles = SALLE.query.join(POSSEDER).join(ESCAPEGAME).filter(POSSEDER.escapegame_id==escapegameid).all()
    new_salle = 0
    for salle in salles:
     if (salle.sal_prec == salle_prec.id):
       new_salle = salle.id
    return redirect(url_for('main.salle', id=id, sessionid=sessionid, escapegameid=escapegameid, salleid=new_salle))
##############################
########  QUESTION  ##########
##############################

@main.route('/debug_questions_get', methods=['POST'])
@login_required
def questions_get():

    questions = QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==1).all()

    return render_template('panel.html', questions=questions)

@main.route('/question_post', methods=['POST'])
@login_required
def question_post():

    id = request.form.get('prodId')
    sessionid = request.form.get('sessionId')
    escapegameid = request.form.get('escapegameId')
    salleid = request.form.get('salleId')
    questionid = request.form.get('questionId')
    timer = request.form.get('timer')
    progression = request.form.get('progression')
    correct = request.form.get('correct')

    question = QUESTION.query.filter_by(id=questionid).first()
    session = SESSION.query.filter_by(id=sessionid).first()

    if(question):
      question.etatQ=True
      db.session.add(question)
      db.session.commit()

    if(session):
      session.tempsRestant=timer
      session.progression=progression
      session.nb_questions+=1
      if(correct=="Oui"):
        session.nb_questions_juste+=1
      db.session.add(session)
    db.session.commit()

    nouveau_enregistrement = ENREGISTREMENT(correct=correct)
    db.session.add(nouveau_enregistrement)
    db.session.commit()
	
    question = QUESTION.query.get(int(questionid))
    enregistrement = ENREGISTREMENT.query.get(int(nouveau_enregistrement.id))
    avoir = AVOIR()
    avoir.enregistrement=enregistrement
    avoir.question=question
    question.enregistrements.append(avoir)
    db.session.add(avoir)
    db.session.commit()

    return redirect(url_for('main.salle', id=id, sessionid=sessionid, escapegameid=escapegameid, salleid=salleid))


@main.route('/debug_get_questions_number', methods=['POST'])
@login_required
def get_questions_number():

    number = QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==3).count()

    return render_template('panel.html', number=number)

@main.route('/debug_get_reponses_question', methods=['POST'])
@login_required
def get_reponses_question():

    reponse = REPONSE.query.join(RATTACHER).join(QUESTION).filter(RATTACHER.question_id==1).all()

    return render_template('panel.html', reponses=reponse)


##############################
########  PANEL  #############
##############################

@main.route('/panel', methods=['POST','GET'])
@login_required
def panel():
    escapegames = ESCAPEGAME.query.filter(ESCAPEGAME.createurE==current_user.nom).all()
    eleves = ELEVE.query.all()
    return render_template('panel.html', escapegames=escapegames, eleves=eleves)
	
@main.route('/edit_escapegame',methods=['POST', 'GET'])
@login_required
def edit_escapegame():
    nom = request.form.get('escapegame', None)
    escapegame = ESCAPEGAME.query.filter_by(nom=nom).first()
	
    if (nom == 'Sélectionnez un escape game' or nom == None):
        flash("Vous n'avez pas sélectionné d'escape game.")
        return redirect(url_for('main.panel'))
	
    return render_template('edit_escapegame.html', escapegameid=escapegame.id)

@main.route('/panel/escapegame/<escapegameid>',methods=['POST', 'GET'])
@login_required
def edit_escapegames(escapegameid):
    escapegame = ESCAPEGAME.query.filter_by(id=escapegameid).first()
    salles = SALLE.query.join(POSSEDER).join(ESCAPEGAME).filter(POSSEDER.escapegame_id==escapegameid).all()
	
    return render_template('edit_escapegames.html', escapeG=escapegame.nom, salles=salles, escapegameid=escapegame.id)
	
@main.route('/panel/escapegame/<escapegameid>/salle/<salleid>',methods=['POST', 'GET'])
@login_required
def edit_salle(escapegameid, salleid):
    salle = SALLE.query.filter_by(id=salleid).first()
    questions = QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
    reponses = REPONSE.query.join(RATTACHER).join(QUESTION).join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
    rattachers = RATTACHER.query.join(QUESTION).join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
	
    return render_template('edit_salle.html', escapegameid=escapegameid, salleid=salleid, nom=salle.nomSal, reponses=reponses, questions=questions, rattachers=rattachers)

@main.route('/panel/escapegame/<id>/ajouter_salle/', methods=['POST', 'GET'])
@login_required
def ajouter_salle(id):

    return render_template('ajouter_salle.html', id=id)

@main.route('/ajouter_salle_post', methods=['POST'])
@login_required
def ajouter_salle_post():

    form_salle_nom = request.form.get('nomSal')
    form_sallePrec_nom = request.form.get('nomSalPrec')
    formimageSal = request.form.get('imageSal')
    form_escape_id = request.form.get('escapegameid')


    sal_prec = SALLE.query.filter_by(nomSal=form_sallePrec_nom).first()
    escapegame = ESCAPEGAME.query.filter_by(id=form_escape_id).first()
	
    if (form_salle_nom==""):
        flash("Vous devez entrer un nom pour votre salle.")
        return redirect(url_for('main.ajouter_salle'))
    elif (form_sallePrec_nom==""):
        flash("Vous devez entrer le nom de votre salle précédente.")
        return redirect(url_for('main.ajouter_salle'))
    elif (formimageSal==""):
        flash("Vous devez entrer l'URL d'une image de 800x480.")
        return redirect(url_for('main.ajouter_salle'))
	
    if not(sal_prec):
      flash("La salle précédente n'existe pas !")
      return redirect(url_for('main.ajouter_salle', id=escapegame.id))

    nouvelle_salle = SALLE(nomSal=form_salle_nom, sal_prec=sal_prec.id, image=formimageSal)
    db.session.add(nouvelle_salle)
    db.session.commit()
	
    salle = SALLE.query.get(int(nouvelle_salle.id))
    posseder = POSSEDER()
    posseder.salle=salle
    posseder.escapegame=escapegame
    escapegame.salles.append(posseder)
    db.session.add(posseder)
    db.session.commit()
    return redirect(url_for('main.edit_escapegames', escapegameid=escapegame.id))

@main.route('/supprimer_salle', methods=['POST'])
@login_required
def supprimer_salle():
    id = request.form.get('id')
    escapegameid = request.form.get('escapegameid')
    salle = SALLE.query.get(int(id))
    SALLE.query.filter(SALLE.id == id).delete()
    POSSEDER.query.filter(POSSEDER.salle_id == id and POSSEDER.escapegame_id == escapegameid).delete()
    questions = QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id == salle.id).all()
    for q in questions:
      SALLE.query.get(int(id))
      QUESTION.query.get(int(q.id)).delete()
    db.session.commit()

    return redirect(url_for('main.edit_escapegames', escapegameid=escapegameid))

@main.route('/panel/escapegame/<escapegameid>/salle/<salleid>/ajouter_question/', methods=['POST', 'GET'])
@login_required
def ajouter_question(escapegameid, salleid):
    salle = SALLE.query.get(int(salleid))
    questions = QUESTION.query.join(CONTENIR).join(SALLE).filter(CONTENIR.salle_id==salleid).all()
    return render_template('ajouter_question.html', image=salle.image, questions=questions, escapegameid=escapegameid, salleid=salleid)

@main.route('/ajouter_question_post', methods=['POST'])
@login_required
def ajouter_question_post():

    form_titre = request.form.get('titre')
    form_contenu = request.form.get('contenu')
    coordx = request.form.get('coordx')
    coordy = request.form.get('coordy')
    salleid = request.form.get('salleid')
    escapegameid = request.form.get('escapegameid')
    texteR = request.form.get('texte')
    correct = request.form.get('question', None)

    salle = SALLE.query.filter_by(id=salleid).first()
    #escapegame = ESCAPEGAME.query.filter_by(id=escapegameid).first()
	
	
    if (correct == 'Indiquez si votre réponse est juste ou non' or correct == None):
        flash("Vous n'avez pas choisi si votre réponse est juste ou non.")
        return redirect(url_for('main.ajouter_question', escapegameid=escapegameid, salleid=salleid))
    elif (form_titre==""):
        flash("Vous devez entrer un titre pour votre question.")
        return redirect(url_for('main.ajouter_question', escapegameid=escapegameid, salleid=salleid))
    elif (form_contenu==""):
        flash("Vous devez entrer le contenu de votre question.")
        return redirect(url_for('main.ajouter_question', escapegameid=escapegameid, salleid=salleid))
    elif (coordx==""):
        flash("Vous devez déplacer votre bouton.")
        return redirect(url_for('main.ajouter_question', escapegameid=escapegameid, salleid=salleid))
    elif (coordy==""):
        flash("Vous devez déplacer votre bouton.")
        return redirect(url_for('main.ajouter_question', escapegameid=escapegameid, salleid=salleid))
    elif (texteR==""):
        flash("Vous devez indiquer une réponse.")
        return redirect(url_for('main.ajouter_question', escapegameid=escapegameid, salleid=salleid))

    nouvelle_question = QUESTION(titreQ=form_titre, contenuQ=form_contenu, etatQ=False)
    db.session.add(nouvelle_question)
    db.session.commit()
	
    salle = SALLE.query.get(int(salleid))
    question = QUESTION.query.get(int(nouvelle_question.id))
    contenir = CONTENIR()
    contenir.question=question
    contenir.salle=salle
    contenir.coordX=coordx
    contenir.coordY=coordy
    salle.questions.append(contenir)
    db.session.add(contenir)
    db.session.commit()

    nouvelle_reponse = REPONSE(texteR=texteR, correct=correct)
    db.session.add(nouvelle_reponse)
    db.session.commit()
	
    question = QUESTION.query.get(int(nouvelle_question.id))
    reponse = REPONSE.query.get(int(nouvelle_reponse.id))
    rattacher = RATTACHER()
    rattacher.reponse=reponse
    rattacher.question=question
    question.reponses.append(rattacher)
    db.session.add(rattacher)
    db.session.commit()
    return redirect(url_for('main.edit_salle', escapegameid=escapegameid, salleid=salleid))

@main.route('/supprimer_question', methods=['POST'])
@login_required
def supprimer_question():
    id = request.form.get('id')
    escapegameid = request.form.get('escapegameid')
    salleid = request.form.get('salleid')
    question = QUESTION.query.get(int(id))
    salle = SALLE.query.get(int(salleid))
    QUESTION.query.filter(QUESTION.id == id).delete()
    CONTENIR.query.filter(CONTENIR.question_id == id and CONTENIR.salle_id == salleid).delete()
    reponses = REPONSE.query.join(RATTACHER).join(QUESTION).filter(RATTACHER.question_id == id).all()
    for r in reponses:
      QUESTION.query.get(int(id))
      REPONSE.query.get(int(r.id)).delete()
    db.session.commit()

    return redirect(url_for('main.edit_salle', escapegameid=escapegameid, salleid=salleid))

@main.route('/panel/escapegame/<escapegameid>/salle/<salleid>/question/<questionid>',methods=['POST', 'GET'])
@login_required
def edit_question(escapegameid, salleid, questionid):
    salle = SALLE.query.filter_by(id=salleid).first()
    question = QUESTION.query.filter_by(id=questionid).first()
    reponses = REPONSE.query.join(RATTACHER).join(QUESTION).filter(RATTACHER.question_id==questionid).all()
	
    return render_template('edit_question.html', escapegameid=escapegameid, salleid=salleid, questionid=questionid, titre=question.titreQ, contenu=question.contenuQ, reponses=reponses)

@main.route('/panel/escapegame/<escapegameid>/salle/<salleid>/question/<questionid>/ajouter_reponse', methods=['POST', 'GET'])
@login_required
def ajouter_reponse(escapegameid, salleid, questionid):

    return render_template('ajouter_reponse.html', escapegameid=escapegameid, salleid=salleid, questionid=questionid)

@main.route('/ajouter_reponse_post', methods=['POST'])
@login_required
def ajouter_reponse_post():

    salleid = request.form.get('salleid')
    escapegameid = request.form.get('escapegameid')
    questionid = request.form.get('questionid')
    texteR = request.form.get('texte')
    correct = request.form.get('question', None)

    question = QUESTION.query.filter_by(id=questionid).first()
    #escapegame = ESCAPEGAME.query.filter_by(id=escapegameid).first()
	
	
    if (correct == 'Indiquez si votre réponse est juste ou non' or correct == None):
        flash("Vous n'avez pas choisi si votre réponse est juste ou non.")
        return redirect(url_for('main.ajouter_reponse', escapegameid=escapegameid, salleid=salleid, questionid=questionid))
    elif (texteR==""):
        flash("Vous devez entrer une réponse pour votre question.")
        return redirect(url_for('main.ajouter_reponse', escapegameid=escapegameid, salleid=salleid, questionid=questionid))

    nouvelle_reponse = REPONSE(texteR=texteR, correct=correct)
    db.session.add(nouvelle_reponse)
    db.session.commit()
	
    question = QUESTION.query.get(int(question.id))
    reponse = REPONSE.query.get(int(nouvelle_reponse.id))
    rattacher = RATTACHER()
    rattacher.reponse=reponse
    rattacher.question=question
    question.reponses.append(rattacher)
    db.session.add(rattacher)
    db.session.commit()
    return redirect(url_for('main.edit_question', escapegameid=escapegameid, salleid=salleid, questionid=questionid))

@main.route('/supprimer_reponse', methods=['POST'])
@login_required
def supprimer_reponse():
    id = request.form.get('id')
    escapegameid = request.form.get('escapegameid')
    salleid = request.form.get('salleid')
    questionid = request.form.get('salleid')
    reponse = REPONSE.query.get(int(id))
    REPONSE.query.filter(REPONSE.id == id).delete()
    RATTACHER.query.filter(RATTACHER.reponse_id == id and RATTACHER.question_id == questionid).delete()
    #enregistrements = ENREGISTREMENT.query.join(AVOIR).join(REPONSE).filter(AVOIR.reponse_id == id).all()
    #for e in enregistrements:
      #REPONSE.query.get(int(id))
      #ENREGISTREMENT.query.get(int(e.id)).delete()
    db.session.commit()

    return redirect(url_for('main.edit_question', escapegameid=escapegameid, salleid=salleid, questionid=questionid))

@main.route('/init_utilisateur',methods=['POST', 'GET'])
@login_required
def init_utilisateur():
    return render_template('init_utilisateur.html')

@main.route('/new_eleve', methods=['POST'])
@login_required
def new_eleve():
    email = request.form.get('email')
    password = request.form.get('password')
    classe = request.form.get('classe')
    nom = request.form.get('nom')
	
    if (email==""):
        flash("Vous devez entrer email valide.")
        return redirect(url_for('main.init_utilisateur'))
    elif (password==""):
        flash("Vous devez entrer un mot de passe.")
        return redirect(url_for('main.init_utilisateur'))
    elif (classe==""):
        flash("Vous devez entrer votre classe.")
        return redirect(url_for('main.init_utilisateur'))
    elif (nom==""):
        flash("Vous devez entrer votre nom.")
        return redirect(url_for('main.init_utilisateur'))
	
    eleve_exist = ELEVE.query.filter_by(nom=nom).first()
    if (eleve_exist):
        flash("Cette élève possède déjà un compte.")
        return redirect(url_for('main.init_utilisateur'))

    new_eleve = ELEVE(email=email, nom=nom, password=generate_password_hash(password, method='sha256'), classe=classe)

    # Ajouter la nouvelle session à la base de donnée
    db.session.add(new_eleve)
    db.session.commit()

    return redirect(url_for('main.index'))
	
@main.route('/supprimer_eleve', methods=['POST'])
@login_required
def supprimer_eleve():
    eleve_nom = request.form.get("eleve", None)
    if (eleve_nom==None or eleve_nom=="Sélectionnez un élève"):
        flash("Vous n'avez pas sélectionné d'élève.")
        return redirect(url_for('main.panel'))
    eleve = ELEVE.query.filter_by(nom=eleve_nom).first()
    ELEVE.query.filter(ELEVE.id == eleve.id).delete()
    APPARTENIR.query.filter(APPARTENIR.user_id == eleve.id).delete()
    db.session.commit()

    return redirect(url_for('main.panel'))
	
##############################
#########   PAUSE    #########
##############################

@main.route('/pause',methods=['POST', 'GET'])
@login_required
def pause():
    escapegameid = request.form.get('escapegameId')
    sessionid = request.form.get('sessionId')

    timer = request.form.get('timer')
    progression = request.form.get('progression')
	
    session = SESSION.query.filter_by(id=sessionid).first()
    if(session):
      session.progression=progression
      session.tempsRestant=timer
      db.session.add(session)
      db.session.commit()

    return redirect(url_for('main.index'))

##############################
#######   FIN_PARTIE    ######
##############################

@main.route('/session/<sessionid>/escapegame/<escapegameid>/fin_partie',methods=['POST', 'GET'])
@login_required
def fin_partie(sessionid, escapegameid):

    enregistrements = ENREGISTREMENT.query.join(AVOIR).join(QUESTION).join(CONTENIR).join(SALLE).join(POSSEDER).join(ESCAPEGAME).join(ATTACHER).join(SESSION).filter(ATTACHER.session_id==sessionid and ATTACHER.escapegame_id==escapegameid).all()
    ATTACHER.query.filter(ATTACHER.session_id == sessionid and ATTACHER.escapegame_id==escapegameid).delete()
    for e in enregistrements:
      ENREGISTREMENT.query.filter(ENREGISTREMENT.id == e.id).delete()
      AVOIR.query.filter(AVOIR.enregistrement_id == e.id).delete()
      ENREGISTRER.query.filter(ENREGISTRER.enregistrement_id == e.id).delete()
    return render_template('finPartie.html', enregistrements=enregistrements)

@main.route('/fin_partie_stats',methods=['POST', 'GET'])
@login_required
def fin_partie_stats():

    nbQuestions = request.form.get('nbQuestions')
    nbQuestionsRepondues = request.form.get('nbQuestionsRepondues')
    escapegameid = request.form.get('escapegameId')
    sessionid = request.form.get('sessionId')

    timer = request.form.get('timer')
    progression = request.form.get('progression')
	

    session = SESSION.query.filter_by(id=sessionid).first()
    questions = QUESTION.query.join(CONTENIR).join(SALLE).join(POSSEDER).join(ESCAPEGAME).join(ATTACHER).join(SESSION).filter(ATTACHER.session_id==sessionid and ATTACHER.escapegame_id==escapegameid).all()

    if(session):
      #session.tempsRestant=timer
      #session.progression=progression
      session.etatS="Fini"
      note = session.nb_questions_juste * 20 / session.nb_questions;
      session.note=note
      #session.progression=progression
      session.dateFin=datetime.datetime.now()
      for question in questions:
        question.etatQ=False
      db.session.add(session)
      db.session.commit()

    enregistrements = ENREGISTREMENT.query.join(AVOIR).join(QUESTION).join(CONTENIR).join(SALLE).join(POSSEDER).join(ESCAPEGAME).join(ATTACHER).join(SESSION).filter(ATTACHER.session_id==sessionid and ATTACHER.escapegame_id==escapegameid).all()
	

    for e in enregistrements:
      session = SESSION.query.get(int(sessionid))
      enregistrement = ENREGISTREMENT.query.get(int(e.id))
      enregistrer = ENREGISTRER()
      enregistrer.enregistrement=enregistrement
      enregistrer.session=session
      session.enregistrements.append(enregistrer)
      db.session.add(enregistrer)
      db.session.commit()

    return redirect(url_for('main.fin_partie', nbQuestions=nbQuestions, nbQuestionsRepondues=nbQuestionsRepondues, escapegameid=escapegameid, sessionid=sessionid))

##############################
######   STATISTIQUE    ######
##############################

@main.route('/statistique')
@login_required
def statistique():
    sessions_last = SESSION.query.join(RELIER).join(GROUPE).join(APPARTENIR).join(UTILISATEUR).filter(SESSION.etatS == "Fini" and APPARTENIR.user_id == current_user.id).order_by(SESSION.id.desc()).limit(5).all()
    groupes = GROUPE.query.join(APPARTENIR).join(UTILISATEUR).filter(APPARTENIR.user_id == current_user.id).all()
    reliers = RELIER.query.join(GROUPE).join(APPARTENIR).join(UTILISATEUR).filter(APPARTENIR.user_id == current_user.id).all()
    return render_template('statistique.html', name=current_user.nom, sessions=sessions_last, groupes=groupes, reliers=reliers)

@main.route('/statistiques',methods=['POST', 'GET'])
@login_required
def statistiques():
    sessions = SESSION.query.join(RELIER).join(GROUPE).join(APPARTENIR).join(UTILISATEUR).filter(SESSION.etatS == "Fini" and APPARTENIR.user_id == current_user.id).all()
    groupes = GROUPE.query.join(APPARTENIR).join(UTILISATEUR).filter(APPARTENIR.user_id == current_user.id).all()
    reliers = RELIER.query.join(GROUPE).join(APPARTENIR).join(UTILISATEUR).filter(APPARTENIR.user_id == current_user.id).all()
    return render_template('statistiques.html', name=current_user.nom, sessions=sessions, groupes=groupes, reliers=reliers)